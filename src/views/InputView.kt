package com.simfootball.isfl.views

import kotlinx.html.*

fun HTML.inputView() {
    head {
        title { +"Update Auditor" }
    }
    body {
        postForm {
            label {
                htmlFor = "input-csv"
                +"csv"
            }
            br {}
            textArea {
                name = "csv"
                id = "input-csv"
            }
            br {}
            button(type = ButtonType.submit, name = "league") {
                value = "DSFL"
                +"DSFL"
            }
            button(type = ButtonType.submit, name = "league") {
                value = "ISFL"
                +"ISFL"
            }
        }
    }
}
