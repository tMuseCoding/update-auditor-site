package model

class Team(val name: String, val forumId: String, val simId: String)

fun getNSFLTeamList() = NSFLTeam.values().map { Team(it.name, it.forumId, it.simId) }

fun getDSFLTeamList() = DSFLTeam.values().map { Team(it.name, it.forumId, it.simId) }

fun getTeamById(id: String): String{
    var team: String
    team = NSFLTeam.values().find { it.forumId == id}?.name ?: ""

    if (team.isEmpty()) {
        team = DSFLTeam.values().find { it.forumId == id}?.name ?: "TEAM NOT FOUND"
    }

    return team
}

enum class NSFLTeam(val forumId: String, val simId: String) {
    BALTIMORE_HAWKS("83", "BAL"),
    CHICAGO_BUTCHERS("142", "CHI"),
    BERLIN_FIRE_SALAMANDERS("369","BER"),
    COLORADO_YETI("67", "COL"),
    PHILADELPHIA_LIBERTY("145", "PHI"),
    YELLOWKNIFE_WRAITHS("69", "YKW"),
    ARIZONA_OUTLAWS("86", "ARI"),
    AUSTIN_COPPERHEADS("128", "AUS"),
    NEW_ORLEANS_SECOND_LINE("148", "NOLA"),
    NEW_YORK_SILVERBACKS("372","NYS"),
    ORANGE_COUNTY_OTTERS("65", "OCO"),
    SAN_JOSE_SABERCATS("63", "SJS"),
    SARASOTA_SAILFISH("307", "SAR"),
    HONOLULU_HAHALUA("309", "HON")
}

enum class DSFLTeam(val forumId: String, val simId: String) {
    BONDI_BEACH_BUCCANEERS("196", "BBB"),
    KANSAS_CITY_COYOTES("194", "KCC"),
    PORTLAND_PYTHONS("200", "POR"),
    NORFOLK_SEAWOLVES("198", "NOR"),
    MINNESOTA_GREY_DUCKS("192", "MINN"),
    TIJUANA_LUCHADORES("190", "TIJ"),
    DALLAS_BIRDDOGS("298", "DAL"),
    LONDON_ROYALS("295", "LON")
}
